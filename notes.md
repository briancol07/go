# Go 

* Designed by google 
* Compiled, so very fast
* No run time required 
* Cocurrency out of the box 
  * More than 1 process at the time 
* Designed for readability and usability 
* Compiles for Windows, Macintosh, linux and others 
* The philosophy is " one problem, one solution"
