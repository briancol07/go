package main

import ("packageone/packageone")

var myVar = "myVar"

// variables 
// Declare a package level variable for the main package 
// package named myVar 

// Declare block variable for the main function 
// called blockvar

// Declare a package level variable in the packageone 
// package name PackageVar

// Create an exported funtion in packageone calle PrintMe

// in the main function, print out the values of myVar,
// blockVar, and PackageVar on one line using the PrintMe in packageone

func main(){
  var blockVar = "blockVar"
  packageone.PrintMe(myVar,blockVar,packageone.PackageVar)


}
