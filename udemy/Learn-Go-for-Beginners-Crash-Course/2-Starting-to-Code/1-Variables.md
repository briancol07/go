# Variables 


## Long way 

``` go 
// one way --> Declare then assign 
var num int 
num = 2 

// another way 

var num1 = 5

// other way  -> This way we let the motor of go to infer the type 
num2 = 5 
```

You must use the variables or the compiler will give you an error 

* No
  * No start with number
  * 
* Yes (start)
  * Letter 
  * Underscore 
