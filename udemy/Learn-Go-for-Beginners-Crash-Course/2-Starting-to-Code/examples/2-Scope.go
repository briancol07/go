package main
 
import "fmt"

// Outside main are global variables 
// Here in golang are call package variable 

var one = "two"

func main(){
  // block level variable 
  // could be shadow variable 
  var one = "One"
  fmt.Println(one)

  myFunc()
}

func myFunc(){
  // one is local to the function 
  var one = "The number one"

  fmt.Println(one)
}

func myFunc2(){
  fmt.Println(one)
}
