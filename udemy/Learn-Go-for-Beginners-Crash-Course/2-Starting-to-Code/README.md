# Starting to Code
 

* Variables 
* Review the built in types, and how to choose the right one 
* Learn how to get input from user 
* Learn how to display results
* Create a couple of programs we'll be using throughout the course 
* Eliza, Hammurabi
* Using these to show how programs works

## Overview 

* [1-Variables.md](./1-Variables.md)
* [2-Scope.md](./2-Scope.md)
* [challenges](./challenges)
  * [1-Scopes.go](./challenges/1-Scopes.go)
* [examples](./examples)
  * [1-Guess-Number.go](./examples/1-Guess-Number.go)
  * [2-Scope.go](./examples/2-Scope.go)
