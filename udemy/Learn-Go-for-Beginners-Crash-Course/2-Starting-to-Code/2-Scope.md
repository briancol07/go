# Scope

```
go mod init nameOfApp
```

this will create a file call go mod 

then create a folder Packageone 

```
---
 |-> main.go
 |-> packageOne
      |-> package.go
```
inside package.go create 2 variable a PublicVar and privateVar

## Access variables on Package 

To access the variables inside packageOne

``` go 

import ( "fmt" "nameOfApp/packageOne")

newString := packageOne.PublicVar

```

## Important 

> With capital letters the var can be use outside the package 

PublicVar could be used but privateVar no

the same with functions 


