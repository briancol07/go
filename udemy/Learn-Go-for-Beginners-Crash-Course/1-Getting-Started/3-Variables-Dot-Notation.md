# Variables and Dot notation 

## Comments 

```go 
// single line 
/* multi line */
```

## Variables 

you cant have unused variables in your program 

```go 

//var name type  
var name string 
name = "pepito"

// assignment operator 

secondName := "juan"
```

The assignment operator check the type and give that type to the variable 

## Dot notation 

To access all the modules you use the dot like :

``` go 
fmt.Println()
```


