# Structure of Go Program 

## Imports 

there are many imports like fmt 

```go
import "fmt"
```
> fmt = format 


## Println vs Print

Print show in screen in the same line but Printline set the cursor in a new line. 

## Functions 

```go 
func sayHelloWorld(vari string){
  fmt.Println(vari)
}
``` 

> var is a keyword 

see [function](./examples/functions.go)
