# Hello world 

## Run Code 

```bash 
go run file.go
```
## Important things 

```go 

package main

import "fmt"

```
## Function Main 
``` go 
func main() {
  fmt.Println("Hello world")
}
```
see [hello-world](./examples/1-Hello-World.go)
