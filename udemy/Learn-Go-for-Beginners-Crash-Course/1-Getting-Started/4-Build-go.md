# Build Go program (Compile) 

## Linux or mac 

``` 
go guild -o nameExe program.go
```

## To run it 

```
./nameExe
```

## Windows 

```
go build -o name.exe program.go 
```


